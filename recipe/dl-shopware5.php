<?php
/***
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Paul Ilea <p.ilea@die-lobby.de>, Die Lobby Werbeagentur GmbH
 *
 ***/

namespace Deployer;

require_once 'recipe/common.php';

/**
 * DocumentRoot / WebRoot for the Shopware installation
 */
set('allow_anonymous_stats', false);
set('shopware_webroot', 'public');

/**
 * Main Shopware task
 */
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
])->desc('Deploy your project');
after('deploy', 'success');

/**
 * Shared directories
 */
set('shared_dirs', [
    'files',
    'media'
]);

/**
 * Shared files
 */
set('shared_files', [
    '.env'
]);

/**
 * Writeable directories
 */
set('writable_dirs', []);
