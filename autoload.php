<?php
/* (c) 2020 Paul Ilea <p.ilea@die-lobby.de>, Die Lobby Werbeagentur GmbH
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__);
